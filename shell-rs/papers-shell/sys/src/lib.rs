// Generated by gir (https://github.com/gtk-rs/gir @ eb5be4f1bafe)
// from ../../ev-girs (@ 057cc645dcee+)
// from ../../gir-files (@ 20031a537e40)
// DO NOT EDIT

#![allow(non_camel_case_types, non_upper_case_globals, non_snake_case)]
#![allow(
    clippy::approx_constant,
    clippy::type_complexity,
    clippy::unreadable_literal,
    clippy::upper_case_acronyms
)]
#![cfg_attr(docsrs, feature(doc_cfg))]

use gio_sys as gio;
use glib_sys as glib;
use gobject_sys as gobject;
use papers_document_sys as papers_document;
use papers_view_sys as papers_view;

#[allow(unused_imports)]
use libc::{
    c_char, c_double, c_float, c_int, c_long, c_short, c_uchar, c_uint, c_ulong, c_ushort, c_void,
    intptr_t, off_t, size_t, ssize_t, time_t, uintptr_t, FILE,
};
#[cfg(unix)]
#[allow(unused_imports)]
use libc::{dev_t, gid_t, pid_t, socklen_t, uid_t};

#[allow(unused_imports)]
use glib::{gboolean, gconstpointer, gpointer, GType};

// Enums
pub type PpsWindowRunMode = c_int;
pub const PPS_WINDOW_MODE_NORMAL: PpsWindowRunMode = 0;
pub const PPS_WINDOW_MODE_FULLSCREEN: PpsWindowRunMode = 1;
pub const PPS_WINDOW_MODE_PRESENTATION: PpsWindowRunMode = 2;
pub const PPS_WINDOW_MODE_START_VIEW: PpsWindowRunMode = 3;
pub const PPS_WINDOW_MODE_ERROR_VIEW: PpsWindowRunMode = 4;
pub const PPS_WINDOW_MODE_PASSWORD_VIEW: PpsWindowRunMode = 5;

// Records
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsApplicationClass {
    pub parent_class: adw::AdwApplicationClass,
}

impl ::std::fmt::Debug for PpsApplicationClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsApplicationClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsMetadataClass {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsMetadataClass = _PpsMetadataClass;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectIface {
    pub parent_iface: gobject::GTypeInterface,
}

impl ::std::fmt::Debug for PpsObjectIface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectIface @ {self:p}"))
            .field("parent_iface", &self.parent_iface)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectManagerClientClass {
    pub parent_class: gio::GDBusObjectManagerClientClass,
}

impl ::std::fmt::Debug for PpsObjectManagerClientClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectManagerClientClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsObjectManagerClientPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsObjectManagerClientPrivate = _PpsObjectManagerClientPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectProxyClass {
    pub parent_class: gio::GDBusObjectProxyClass,
}

impl ::std::fmt::Debug for PpsObjectProxyClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectProxyClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsObjectProxyPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsObjectProxyPrivate = _PpsObjectProxyPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectSkeletonClass {
    pub parent_class: gio::GDBusObjectSkeletonClass,
}

impl ::std::fmt::Debug for PpsObjectSkeletonClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectSkeletonClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsObjectSkeletonPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsObjectSkeletonPrivate = _PpsObjectSkeletonPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersApplicationIface {
    pub parent_iface: gobject::GTypeInterface,
    pub handle_get_window_list: Option<
        unsafe extern "C" fn(
            *mut PpsPapersApplication,
            *mut gio::GDBusMethodInvocation,
        ) -> gboolean,
    >,
}

impl ::std::fmt::Debug for PpsPapersApplicationIface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersApplicationIface @ {self:p}"))
            .field("parent_iface", &self.parent_iface)
            .field("handle_get_window_list", &self.handle_get_window_list)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersApplicationProxyClass {
    pub parent_class: gio::GDBusProxyClass,
}

impl ::std::fmt::Debug for PpsPapersApplicationProxyClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersApplicationProxyClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsPapersApplicationProxyPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsPapersApplicationProxyPrivate = _PpsPapersApplicationProxyPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersApplicationSkeletonClass {
    pub parent_class: gio::GDBusInterfaceSkeletonClass,
}

impl ::std::fmt::Debug for PpsPapersApplicationSkeletonClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersApplicationSkeletonClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsPapersApplicationSkeletonPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsPapersApplicationSkeletonPrivate = _PpsPapersApplicationSkeletonPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersWindowIface {
    pub parent_iface: gobject::GTypeInterface,
    pub closed: Option<unsafe extern "C" fn(*mut PpsPapersWindow)>,
    pub document_loaded: Option<unsafe extern "C" fn(*mut PpsPapersWindow, *const c_char)>,
}

impl ::std::fmt::Debug for PpsPapersWindowIface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersWindowIface @ {self:p}"))
            .field("parent_iface", &self.parent_iface)
            .field("closed", &self.closed)
            .field("document_loaded", &self.document_loaded)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersWindowProxyClass {
    pub parent_class: gio::GDBusProxyClass,
}

impl ::std::fmt::Debug for PpsPapersWindowProxyClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersWindowProxyClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsPapersWindowProxyPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsPapersWindowProxyPrivate = _PpsPapersWindowProxyPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersWindowSkeletonClass {
    pub parent_class: gio::GDBusInterfaceSkeletonClass,
}

impl ::std::fmt::Debug for PpsPapersWindowSkeletonClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersWindowSkeletonClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsPapersWindowSkeletonPrivate {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsPapersWindowSkeletonPrivate = _PpsPapersWindowSkeletonPrivate;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsSidebarPageInterface {
    pub base_iface: gobject::GTypeInterface,
    pub support_document: Option<
        unsafe extern "C" fn(*mut PpsSidebarPage, *mut papers_document::PpsDocument) -> gboolean,
    >,
    pub set_model:
        Option<unsafe extern "C" fn(*mut PpsSidebarPage, *mut papers_view::PpsDocumentModel)>,
}

impl ::std::fmt::Debug for PpsSidebarPageInterface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsSidebarPageInterface @ {self:p}"))
            .field("base_iface", &self.base_iface)
            .field("support_document", &self.support_document)
            .field("set_model", &self.set_model)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsWindowClass {
    pub parent_class: adw::AdwApplicationWindowClass,
}

impl ::std::fmt::Debug for PpsWindowClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsWindowClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

// Classes
#[repr(C)]
pub struct PpsApplication {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsApplication {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsApplication @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsMetadata {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsMetadata {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsMetadata @ {self:p}")).finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectManagerClient {
    pub parent_instance: gio::GDBusObjectManagerClient,
    pub priv_: *mut PpsObjectManagerClientPrivate,
}

impl ::std::fmt::Debug for PpsObjectManagerClient {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectManagerClient @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectProxy {
    pub parent_instance: gio::GDBusObjectProxy,
    pub priv_: *mut PpsObjectProxyPrivate,
}

impl ::std::fmt::Debug for PpsObjectProxy {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectProxy @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsObjectSkeleton {
    pub parent_instance: gio::GDBusObjectSkeleton,
    pub priv_: *mut PpsObjectSkeletonPrivate,
}

impl ::std::fmt::Debug for PpsObjectSkeleton {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsObjectSkeleton @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersApplicationProxy {
    pub parent_instance: gio::GDBusProxy,
    pub priv_: *mut PpsPapersApplicationProxyPrivate,
}

impl ::std::fmt::Debug for PpsPapersApplicationProxy {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersApplicationProxy @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersApplicationSkeleton {
    pub parent_instance: gio::GDBusInterfaceSkeleton,
    pub priv_: *mut PpsPapersApplicationSkeletonPrivate,
}

impl ::std::fmt::Debug for PpsPapersApplicationSkeleton {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersApplicationSkeleton @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersWindowProxy {
    pub parent_instance: gio::GDBusProxy,
    pub priv_: *mut PpsPapersWindowProxyPrivate,
}

impl ::std::fmt::Debug for PpsPapersWindowProxy {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersWindowProxy @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsPapersWindowSkeleton {
    pub parent_instance: gio::GDBusInterfaceSkeleton,
    pub priv_: *mut PpsPapersWindowSkeletonPrivate,
}

impl ::std::fmt::Debug for PpsPapersWindowSkeleton {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPapersWindowSkeleton @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsWindow {
    pub base_instance: adw::AdwApplicationWindow,
}

impl ::std::fmt::Debug for PpsWindow {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsWindow @ {self:p}"))
            .field("base_instance", &self.base_instance)
            .finish()
    }
}

// Interfaces
#[repr(C)]
pub struct PpsObject {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsObject {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "PpsObject @ {self:p}")
    }
}

#[repr(C)]
pub struct PpsPapersApplication {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsPapersApplication {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "PpsPapersApplication @ {self:p}")
    }
}

#[repr(C)]
pub struct PpsPapersWindow {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsPapersWindow {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "PpsPapersWindow @ {self:p}")
    }
}

#[repr(C)]
pub struct PpsSidebarPage {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsSidebarPage {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "PpsSidebarPage @ {self:p}")
    }
}

#[link(name = "ppsshell-4.0")]
#[link(name = "ppsview-4.0")]
#[link(name = "ppsdocument-4.0")]
extern "C" {

    //=========================================================================
    // PpsWindowRunMode
    //=========================================================================
    pub fn pps_window_run_mode_get_type() -> GType;

    //=========================================================================
    // PpsApplication
    //=========================================================================
    pub fn pps_application_get_type() -> GType;
    pub fn pps_application_new() -> *mut PpsApplication;
    pub fn pps_application_get_n_windows(application: *mut PpsApplication) -> c_uint;
    pub fn pps_application_open_start_view(application: *mut PpsApplication);
    pub fn pps_application_open_uri_at_dest(
        application: *mut PpsApplication,
        uri: *const c_char,
        dest: *mut papers_document::PpsLinkDest,
        mode: PpsWindowRunMode,
    );
    pub fn pps_application_open_uri_list(
        application: *mut PpsApplication,
        files: *mut gio::GListModel,
    );

    //=========================================================================
    // PpsMetadata
    //=========================================================================
    pub fn pps_metadata_get_type() -> GType;
    pub fn pps_metadata_new(file: *mut gio::GFile) -> *mut PpsMetadata;
    pub fn pps_metadata_is_file_supported(file: *mut gio::GFile) -> gboolean;
    pub fn pps_metadata_get_boolean(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: *mut gboolean,
    ) -> gboolean;
    pub fn pps_metadata_get_double(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: *mut c_double,
    ) -> gboolean;
    pub fn pps_metadata_get_int(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: *mut c_int,
    ) -> gboolean;
    pub fn pps_metadata_get_string(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: *mut *const c_char,
    ) -> gboolean;
    pub fn pps_metadata_has_key(metadata: *mut PpsMetadata, key: *const c_char) -> gboolean;
    pub fn pps_metadata_is_empty(metadata: *mut PpsMetadata) -> gboolean;
    pub fn pps_metadata_set_boolean(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: gboolean,
    ) -> gboolean;
    pub fn pps_metadata_set_double(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: c_double,
    ) -> gboolean;
    pub fn pps_metadata_set_int(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: c_int,
    ) -> gboolean;
    pub fn pps_metadata_set_string(
        metadata: *mut PpsMetadata,
        key: *const c_char,
        value: *const c_char,
    ) -> gboolean;

    //=========================================================================
    // PpsObjectManagerClient
    //=========================================================================
    pub fn pps_object_manager_client_get_type() -> GType;
    pub fn pps_object_manager_client_new_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsObjectManagerClient;
    pub fn pps_object_manager_client_new_for_bus_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsObjectManagerClient;
    pub fn pps_object_manager_client_new_for_bus_sync(
        bus_type: gio::GBusType,
        flags: gio::GDBusObjectManagerClientFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsObjectManagerClient;
    pub fn pps_object_manager_client_new_sync(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusObjectManagerClientFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsObjectManagerClient;
    pub fn pps_object_manager_client_get_proxy_type(
        manager: *mut gio::GDBusObjectManagerClient,
        object_path: *const c_char,
        interface_name: *const c_char,
        user_data: gpointer,
    ) -> GType;
    pub fn pps_object_manager_client_new(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusObjectManagerClientFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn pps_object_manager_client_new_for_bus(
        bus_type: gio::GBusType,
        flags: gio::GDBusObjectManagerClientFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );

    //=========================================================================
    // PpsObjectProxy
    //=========================================================================
    pub fn pps_object_proxy_get_type() -> GType;
    pub fn pps_object_proxy_new(
        connection: *mut gio::GDBusConnection,
        object_path: *const c_char,
    ) -> *mut PpsObjectProxy;

    //=========================================================================
    // PpsObjectSkeleton
    //=========================================================================
    pub fn pps_object_skeleton_get_type() -> GType;
    pub fn pps_object_skeleton_new(object_path: *const c_char) -> *mut PpsObjectSkeleton;
    pub fn pps_object_skeleton_set_papers_application(
        object: *mut PpsObjectSkeleton,
        interface_: *mut PpsPapersApplication,
    );
    pub fn pps_object_skeleton_set_papers_window(
        object: *mut PpsObjectSkeleton,
        interface_: *mut PpsPapersWindow,
    );

    //=========================================================================
    // PpsPapersApplicationProxy
    //=========================================================================
    pub fn pps_papers_application_proxy_get_type() -> GType;
    pub fn pps_papers_application_proxy_new_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersApplicationProxy;
    pub fn pps_papers_application_proxy_new_for_bus_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersApplicationProxy;
    pub fn pps_papers_application_proxy_new_for_bus_sync(
        bus_type: gio::GBusType,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersApplicationProxy;
    pub fn pps_papers_application_proxy_new_sync(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersApplicationProxy;
    pub fn pps_papers_application_proxy_new(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn pps_papers_application_proxy_new_for_bus(
        bus_type: gio::GBusType,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );

    //=========================================================================
    // PpsPapersApplicationSkeleton
    //=========================================================================
    pub fn pps_papers_application_skeleton_get_type() -> GType;
    pub fn pps_papers_application_skeleton_new() -> *mut PpsPapersApplicationSkeleton;

    //=========================================================================
    // PpsPapersWindowProxy
    //=========================================================================
    pub fn pps_papers_window_proxy_get_type() -> GType;
    pub fn pps_papers_window_proxy_new_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersWindowProxy;
    pub fn pps_papers_window_proxy_new_for_bus_finish(
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersWindowProxy;
    pub fn pps_papers_window_proxy_new_for_bus_sync(
        bus_type: gio::GBusType,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersWindowProxy;
    pub fn pps_papers_window_proxy_new_sync(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> *mut PpsPapersWindowProxy;
    pub fn pps_papers_window_proxy_new(
        connection: *mut gio::GDBusConnection,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn pps_papers_window_proxy_new_for_bus(
        bus_type: gio::GBusType,
        flags: gio::GDBusProxyFlags,
        name: *const c_char,
        object_path: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );

    //=========================================================================
    // PpsPapersWindowSkeleton
    //=========================================================================
    pub fn pps_papers_window_skeleton_get_type() -> GType;
    pub fn pps_papers_window_skeleton_new() -> *mut PpsPapersWindowSkeleton;

    //=========================================================================
    // PpsWindow
    //=========================================================================
    pub fn pps_window_get_type() -> GType;
    pub fn pps_window_new() -> *mut PpsWindow;
    pub fn pps_window_focus_view(pps_window: *mut PpsWindow);
    pub fn pps_window_get_dbus_object_path(pps_window: *mut PpsWindow) -> *const c_char;
    pub fn pps_window_get_header_bar(pps_window: *mut PpsWindow) -> *mut adw::AdwHeaderBar;
    pub fn pps_window_get_metadata(pps_window: *mut PpsWindow) -> *mut PpsMetadata;
    pub fn pps_window_get_uri(pps_window: *mut PpsWindow) -> *const c_char;
    pub fn pps_window_handle_annot_popup(
        pps_window: *mut PpsWindow,
        annot: *mut papers_document::PpsAnnotation,
    );
    pub fn pps_window_is_empty(pps_window: *mut PpsWindow) -> gboolean;
    pub fn pps_window_open_uri(
        pps_window: *mut PpsWindow,
        uri: *const c_char,
        dest: *mut papers_document::PpsLinkDest,
        mode: PpsWindowRunMode,
    );
    pub fn pps_window_print_range(pps_window: *mut PpsWindow, first_page: c_int, last_page: c_int);

    //=========================================================================
    // PpsObject
    //=========================================================================
    pub fn pps_object_get_type() -> GType;
    pub fn pps_object_get_papers_application(object: *mut PpsObject) -> *mut PpsPapersApplication;
    pub fn pps_object_get_papers_window(object: *mut PpsObject) -> *mut PpsPapersWindow;
    pub fn pps_object_peek_papers_application(object: *mut PpsObject) -> *mut PpsPapersApplication;
    pub fn pps_object_peek_papers_window(object: *mut PpsObject) -> *mut PpsPapersWindow;

    //=========================================================================
    // PpsPapersApplication
    //=========================================================================
    pub fn pps_papers_application_get_type() -> GType;
    pub fn pps_papers_application_interface_info() -> *mut gio::GDBusInterfaceInfo;
    pub fn pps_papers_application_override_properties(
        klass: *mut gobject::GObjectClass,
        property_id_begin: c_uint,
    ) -> c_uint;
    pub fn pps_papers_application_call_get_window_list(
        proxy: *mut PpsPapersApplication,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn pps_papers_application_call_get_window_list_finish(
        proxy: *mut PpsPapersApplication,
        out_window_list: *mut *mut *mut c_char,
        res: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn pps_papers_application_call_get_window_list_sync(
        proxy: *mut PpsPapersApplication,
        out_window_list: *mut *mut *mut c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn pps_papers_application_complete_get_window_list(
        object: *mut PpsPapersApplication,
        invocation: *mut gio::GDBusMethodInvocation,
        window_list: *const *const c_char,
    );

    //=========================================================================
    // PpsPapersWindow
    //=========================================================================
    pub fn pps_papers_window_get_type() -> GType;
    pub fn pps_papers_window_interface_info() -> *mut gio::GDBusInterfaceInfo;
    pub fn pps_papers_window_override_properties(
        klass: *mut gobject::GObjectClass,
        property_id_begin: c_uint,
    ) -> c_uint;
    pub fn pps_papers_window_emit_closed(object: *mut PpsPapersWindow);
    pub fn pps_papers_window_emit_document_loaded(
        object: *mut PpsPapersWindow,
        arg_uri: *const c_char,
    );

    //=========================================================================
    // PpsSidebarPage
    //=========================================================================
    pub fn pps_sidebar_page_get_type() -> GType;
    pub fn pps_sidebar_page_set_model(
        sidebar_page: *mut PpsSidebarPage,
        model: *mut papers_view::PpsDocumentModel,
    );
    pub fn pps_sidebar_page_support_document(
        sidebar_page: *mut PpsSidebarPage,
        document: *mut papers_document::PpsDocument,
    ) -> gboolean;

    //=========================================================================
    // Other functions
    //=========================================================================
    pub fn pps_get_resource() -> *mut gio::GResource;
    pub fn pps_shell_marshal_VOID__POINTER_POINTER(
        closure: *mut gobject::GClosure,
        return_value: *mut gobject::GValue,
        n_param_values: c_uint,
        param_values: *const gobject::GValue,
        invocation_hint: gpointer,
        marshal_data: gpointer,
    );

}
