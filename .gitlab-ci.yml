include:
    - project: "GNOME/citemplates"
      file: "flatpak/flatpak_ci_initiative.yml"
    - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/f9171e21724bc4e2abeabad5f2d7e2e5cc10cbe3/templates/fedora.yml'

stages:
    - lint
    - prepare
    - build
    - deploy

variables:
    # When branching a stable release, change 'main'
    # to the release number/branch to ensure that
    # a new image will be created, tailored for the
    # stable branch.
    # Could probably also switch away from rawhide,
    # to stable fedora branch as well.
    FDO_DISTRIBUTION_TAG: '2024-05-05.0-main'
    FDO_DISTRIBUTION_VERSION: rawhide

editorconfig:
    stage: lint
    image: alpine:edge
    script:
        - apk add editorconfig-checker
        - ec --disable-indentation --exclude '^.git/'

build.container.fedora:
    extends: '.fdo.container-build@fedora'
    stage: prepare
    variables:
        # no need to pull the whole tree for rebuilding the image
        GIT_STRATEGY: none
        # Expiry sets fdo.expires on the image
        FDO_EXPIRES_AFTER: 8w
        FDO_DISTRIBUTION_PACKAGES: >-
            meson
            appstream
            cairo-devel
            clippy
            dbus-devel
            desktop-file-utils
            djvulibre-devel
            exempi-devel
            gi-docgen
            glib2-devel
            gobject-introspection-devel
            gsettings-desktop-schemas-devel
            gtk4-devel
            itstool
            libadwaita-devel
            libarchive-devel
            libgxps-devel
            libsecret-devel
            libspectre-devel
            libtiff-devel
            nautilus-devel
            poppler-glib-devel
            yelp-tools
            zlib-devel
            rust
            cargo
            git

.setup-meson:
    stage: build
    extends: '.fdo.distribution-image@fedora'
    artifacts:
        when: on_failure
        paths:
            - _build/meson-logs/meson-log.txt
            - _build/meson-logs/testlog.txt
        expire_in: 2 days
    before_script:
        - git submodule init
        - git submodule update --checkout
        - cargo install --path shell-rs/gir
        - export PATH=$PATH:$HOME/.cargo/bin
        - meson setup $MESON_SETUP_OPTIONS --werror --fatal-meson-warnings _build
        - meson compile -C _build update-rust-bindings

meson-dev:
    extends: '.setup-meson'
    variables:
        MESON_SETUP_OPTIONS: "-Dbuildtype=debug"
    script:
        - meson compile -C _build
        - meson compile -C _build cargo-clippy
        - meson test -C _build

meson-rel:
    extends: '.setup-meson'
    variables:
        MESON_SETUP_OPTIONS: "-Dbuildtype=release"
    script:
        - meson compile -C _build

meson-pot:
    extends: '.setup-meson'
    variables:
        MESON_SETUP_OPTIONS: "-Dbuildtype=release"
    script:
        - meson compile -C _build papers-pot

.flatpak-local:
    stage: build
    variables:
        MANIFEST_PATH: "build-aux/flatpak/org.gnome.Papers.json"
        RUNTIME_REPO: "https://nightly.gnome.org/gnome-nightly.flatpakrepo"
        FLATPAK_MODULE: "papers"
        APP_ID: "org.gnome.Papers.Devel"
        BUNDLE: "org.gnome.Papers.Devel.flatpak"
    retry: 2

flatpak@x86_64:
    extends:
        - .flatpak@x86_64
        - .flatpak-local

flatpak@aarch64:
    extends:
        - .flatpak@aarch64
        - .flatpak-local

nightly@x86_64:
  extends: '.publish_nightly'
  needs: ['flatpak@x86_64']

nightly@aarch64:
  extends: '.publish_nightly'
  needs: ['flatpak@aarch64']

pages:
    extends: '.fdo.distribution-image@fedora'
    stage: deploy
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_NAMESPACE == "GNOME"
    script:
        - meson setup _build
        - ninja -C _build
        - mkdir public
        - mv _build/help/reference/libdocument/libppsdocument public/document/
        - mv _build/help/reference/libview/libppsview public/view/
    artifacts:
        paths:
            - public/
        expire_in: 2 days
