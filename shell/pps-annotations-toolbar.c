/* pps-annotations-toolbar.c
 *  this file is part of papers, a gnome document viewer
 *
 * Copyright (C) 2015 Carlos Garcia Campos  <carlosgc@gnome.org>
 *
 * Papers is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Papers is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "pps-annotations-toolbar.h"
#include <papers-document.h>
#include <glib/gi18n.h>

#include <adwaita.h>

enum {
        BEGIN_ADD_ANNOT,
        CANCEL_ADD_ANNOT,
        N_SIGNALS
};

struct _PpsAnnotationsToolbar {
	GtkBox base_instance;

        GtkWidget *text_button;
        GtkWidget *highlight_button;
};

static guint signals[N_SIGNALS];

G_DEFINE_TYPE (PpsAnnotationsToolbar, pps_annotations_toolbar, GTK_TYPE_BOX)

static void
pps_annotations_toolbar_annot_button_toggled (GtkWidget            *button,
                                             PpsAnnotationsToolbar *toolbar)
{
        PpsAnnotationType annot_type;

        if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button))) {
                g_signal_emit (toolbar, signals[CANCEL_ADD_ANNOT], 0, NULL);
                return;
        }

        if (button == toolbar->text_button) {
                annot_type = PPS_ANNOTATION_TYPE_TEXT;
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toolbar->highlight_button), FALSE);
        } else if (button == toolbar->highlight_button) {
                annot_type = PPS_ANNOTATION_TYPE_TEXT_MARKUP;
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toolbar->text_button), FALSE);
        } else {
                g_assert_not_reached ();
        }

        g_signal_emit (toolbar, signals[BEGIN_ADD_ANNOT], 0, annot_type);
}

static gboolean
pps_annotations_toolbar_toggle_button_if_active (PpsAnnotationsToolbar *toolbar,
                                                GtkToggleButton      *button)
{
        if (!gtk_toggle_button_get_active (button))
                return FALSE;

        g_signal_handlers_block_by_func (button,
                                         pps_annotations_toolbar_annot_button_toggled,
                                         toolbar);
        gtk_toggle_button_set_active (button, FALSE);
        g_signal_handlers_unblock_by_func (button,
                                           pps_annotations_toolbar_annot_button_toggled,
                                           toolbar);

        return TRUE;
}

static GtkWidget *
pps_annotations_toolbar_create_toggle_button (PpsAnnotationsToolbar *toolbar,
                                             const gchar          *label,
                                             const gchar          *icon_name)
{
        GtkWidget *button = gtk_toggle_button_new ();
	GtkWidget *button_content = adw_button_content_new ();

	adw_button_content_set_label (ADW_BUTTON_CONTENT (button_content),
				      label);
	adw_button_content_set_icon_name (ADW_BUTTON_CONTENT (button_content),
					  icon_name);

	gtk_button_set_child (GTK_BUTTON (button), button_content);

        g_signal_connect (button, "toggled",
                          G_CALLBACK (pps_annotations_toolbar_annot_button_toggled),
                          toolbar);

        return button;
}

static void
pps_annotations_toolbar_init (PpsAnnotationsToolbar *toolbar)
{
        gtk_orientable_set_orientation (GTK_ORIENTABLE (toolbar), GTK_ORIENTATION_HORIZONTAL);

	gtk_widget_add_css_class (GTK_WIDGET (toolbar), "toolbar");

        toolbar->text_button = pps_annotations_toolbar_create_toggle_button (toolbar,
                               /* Translators: an annotation that looks like a "sticky note" */
                                                                            _("Add Note"),
                                                                            "annotations-text-symbolic");
        gtk_box_append (GTK_BOX(toolbar), toolbar->text_button);

        toolbar->highlight_button = pps_annotations_toolbar_create_toggle_button (toolbar,
                                                                                 _("Highlight Text"),
                                                                                 "marker-symbolic");

        gtk_box_append (GTK_BOX (toolbar), toolbar->highlight_button);
}

static void
pps_annotations_toolbar_class_init (PpsAnnotationsToolbarClass *klass)
{
        GObjectClass *g_object_class = G_OBJECT_CLASS (klass);

        signals[BEGIN_ADD_ANNOT] =
                g_signal_new ("begin-add-annot",
                              G_TYPE_FROM_CLASS (g_object_class),
                              G_SIGNAL_RUN_LAST,
                              0,
                              NULL, NULL,
                              g_cclosure_marshal_VOID__ENUM,
                              G_TYPE_NONE, 1,
                              PPS_TYPE_ANNOTATION_TYPE);

        signals[CANCEL_ADD_ANNOT] =
                g_signal_new ("cancel-add-annot",
                              G_TYPE_FROM_CLASS (g_object_class),
                              G_SIGNAL_RUN_LAST,
                              0,
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE, 0,
                              G_TYPE_NONE);
}

GtkWidget *
pps_annotations_toolbar_new (void)
{
	return GTK_WIDGET (g_object_new (PPS_TYPE_ANNOTATIONS_TOOLBAR, NULL));
}

void
pps_annotations_toolbar_add_annot_finished (PpsAnnotationsToolbar *toolbar)
{
        g_return_if_fail (PPS_IS_ANNOTATIONS_TOOLBAR (toolbar));

        if (pps_annotations_toolbar_toggle_button_if_active (toolbar, GTK_TOGGLE_BUTTON (toolbar->text_button)))
                return;

        pps_annotations_toolbar_toggle_button_if_active (toolbar, GTK_TOGGLE_BUTTON (toolbar->highlight_button));
}
